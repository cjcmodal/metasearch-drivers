<?php


return [
    'id' => 'noticia_fiscal_id',
    'post_modified' => [
        'value'=>'published_at',
        'callbacks' => []
    ],
    'post_content' => 'description',
    'post_title' => 'title',
    'post_year' => 'year_at',
    'post_month' => 'month_at',
    'post_day' => 'day_at',
    'post_resume' => 'resume',
    'post_media' => [
        'value' => 'tumbnail',
        'callbacks' => [ 'extract_media']
    ],
    'post_categories' => [
        'value' => 'categories',
        'callbacks' => [ 'get_categories_array']
    ],
    '697' => [
        'value' => 'categories',
        'callbacks' => [
            //'abcd_explode_values'
        ]
    ],
   
];