<?php


namespace Modalnetworks\MetaSearch\Entities\Noticias;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use function GuzzleHttp\json_decode;

class Clipping
{

    protected $base_url = 'https://noticiasfiscais.com.br/wp-json/mdl/v1/posts';

    protected $query_params = [];
    /**
     * @var GuzzleHttp\Client
     */
    protected $httpClient;

    public function addParams($params)
    {
        $this->query_params = array_replace($this->query_params, $params);
        return $this;
    }

    public function  getParams()
    {
        return $this->query_params;
    }

    public static function all()
    {

        $self = new self;
        $client = $self->getClientGuzzle();

        $response = $client->request('GET', null, [
            'query' => $self->getParams()
        ]);
        $body = $response->getBody()->getContents();
        return json_decode($body, true);
    }



    /**
     * @return Client|GuzzleHttp\Client
     */
    private function getClientGuzzle()
    {
        if (!$this->httpClient) $this->httpClient = new Client(
            ['base_uri' => $this->base_url]
        );
        return $this->httpClient;
    }
}
